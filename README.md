[![MIICT](images/miict-logo.png)](https://www.miict.eu/)
# semantic-models

The repository contains the core MIICT ontology (currently in progress)

The MIICT ontology is used for matching services and user profiles and 
describes the following aspects:

- Information related to services created through the IMMERSE platform, such as Job Posts, Housing Posts, Volunteering Offers, Training Courses etc;
    
- Information related to the migrant profiles, such as their location,  education level, the languages they speak, past experience, skills, expectations as well as their overall activity in the platform (favorite posts and applications) etc.;

- Recommendations that become available to the users by analysing the above information.
